---
name: Sean T Allen
talks:
- 'Scale Independent Python: How to scale your Python application without any code
  changes'
---

Sean T. Allen is vice president of engineering at Wallaroo Labs and a member of the Pony core team. His turn-ons include programming languages, distributed computing, Hiwatt amplifiers, and Fender Telecasters. His turn-offs include mayonnaise, stirring yogurt, and sloppy code. He is one of the authors of Storm Applied.